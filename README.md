# Beer Evolution AI - My very first AI

This was my entry for the the AI Bootcamp (Golem Science Club) competiton.

Uses evolution algorithm instead of backpropagation!

## Requirements:

- Python 3.8
- Numpy
- pandas
- xlrd

## Solution Description:

Evolution AI:

- At the start, generate X random robots with random weights, and test them on the train data
- Rank them in order
- Take top 10, duplicate them until robot count is X again, randomly nudge some weights, and repeat.
- Whenever a robot with better accuracy is found, save the robot to a text file (weights).

## Results:

The best robot (weights available in *Best.txt* ) achieved 57% accuracy, with 4 layers and 16 neurons per layer. Potential improvements include using vectorization, rgularization techniques, and introducing backpropagation instead of random weight nudge.
